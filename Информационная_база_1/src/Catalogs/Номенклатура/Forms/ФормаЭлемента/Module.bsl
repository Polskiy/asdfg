
&НаКлиенте
Процедура АдресФотоНажатие(Элемент, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	
	ДругаяПроцедура = Новый ОписаниеОповещения("ДиалогВыбораЗакрыт", ЭтотОбъект);
	
	ПараметрыДиалога = Новый ПараметрыДиалогаПомещенияФайлов;
	ПараметрыДиалога.Заголовок = "Выберите картинку";
	ПараметрыДиалога.Фильтр = "Изображения|*.png;*.jpg";
	ПараметрыДиалога.МножественныйВыбор = Ложь;
	
	НачатьПомещениеФайлаНаСервер(ДругаяПроцедура,,,,ПараметрыДиалога,УникальныйИдентификатор);
КонецПроцедуры

&НаКлиенте
Процедура ДиалогВыбораЗакрыт(ОписаниеПомещенногоФайла, ДополнительныеПараметры) Экспорт
	Если ОписаниеПомещенногоФайла = Неопределено Тогда
		Возврат;
	КонецЕсли;
	АдресФото = ОписаниеПомещенногоФайла.Адрес;
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	ДвоичныеДанныеФотографии = ПолучитьИзВременногоХранилища(АдресФото);
	ТекущийОбъект.Фотография = Новый ХранилищеЗначения(ДвоичныеДанныеФотографии);
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	ДвоичныеДанныеФотографии = ТекущийОбъект.Фотография.Получить();
	АдресФото = ПоместитьВоВременноеХранилище(ДвоичныеДанныеФотографии,УникальныйИдентификатор);
КонецПроцедуры









